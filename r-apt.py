#!/usr/bin/fab -f
# -*- coding:utf-8; tab-width:4; mode:python -*-
"""
usage: ./r-apt.py cmd:args
sample: ./r-apt.py install:cherokee
"""

import os
import getpass

from fabric import api


# http://markpasc.typepad.com/blog/2010/04/loading-ssh-config-settings-for-fabric.html
def annotate_hosts_with_ssh_config_info(hosts):
    from os.path import expanduser
    from paramiko.config import SSHConfig

    def hostinfo(host, config):
        hive = config.lookup(host)
        if 'hostname' in hive:
            host = hive['hostname']
        if 'user' in hive:
            host = '%s@%s' % (hive['user'], host)
        if 'port' in hive:
            host = '%s:%s' % (host, hive['port'])
        return host

    try:
        config_file = file(expanduser('~/.ssh/config'))
    except IOError:
        pass
    else:
        config = SSHConfig()
        config.parse(config_file)
        keys = [config.lookup(host).get('identityfile', None) for host in hosts]
        keys = [x for x in keys if os.path.exists(x)]
        api.env.key_filename = [expanduser(key) for key in keys if key is not None]
        retval = [hostinfo(host, config) for host in hosts]

    for h in retval:
        print('canonical name: {0}'.format(h))

    return retval


api.env.hosts = annotate_hosts_with_ssh_config_info(api.env.hosts)


def cmd(cmd):
    api.run(cmd)


def sudo(cmd):
    if not api.env.password:
        api.env.password = getpass.getpass('remote sudo password: ')

    api.sudo(cmd)


def update():
    sudo('aptitude update')


def install(pkg):
    sudo('aptitude -y install {0}'.format(pkg))


def remove(pkg):
    sudo('aptitude -y remove {0}'.format(pkg))
